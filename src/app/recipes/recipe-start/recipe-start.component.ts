import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-recipe',
  templateUrl: './recipe-start.component.html',
  styleUrls: ['./recipe-start.component.css']
})
export class RecipeStartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}
